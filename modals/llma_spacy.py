
# Install and download the English NLP model from spaCy
# !pip install -U spacy
# !python -m spacy download en_core_web_md
import spacy
nlp = spacy.load('en_core_web_md')
# nlp = spacy.load('/path/to/en_core_web_md')
def spacy_simplify_text(input_text):
    # Process the input text using spaCy
    doc = nlp(input_text)

    simplified_words = []

    for token in doc:
        # Lemmatize the token and get its base form
        lemma = token.lemma_

        # Append the lemmatized word to the list of simplified words
        simplified_words.append(lemma)

    # Join the list of simplified words into a single string
    simplified_text = ' '.join(simplified_words)
    return simplified_text
