import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

custom_synonyms = None

def load_custom_synonyms():
    global custom_synonyms
    if custom_synonyms is None:
        from modals.synonyms_generator import load_synonyms_dict
        custom_synonyms = load_synonyms_dict()

def nlt_simplify_text_v2(input_text, epoch):
    tokens = word_tokenize(input_text)

    load_custom_synonyms()

    simplified_words = []
    for word, tag in nltk.pos_tag(tokens):
        word_lower = word.lower()
        if word_lower in custom_synonyms:
            simplified_words.append(random.choice(custom_synonyms[word_lower]))
            continue

        if tag.startswith('NNP'):
            pos = None
        elif tag.startswith('NN'):
            pos = wordnet.NOUN
        elif tag.startswith('VB'):
            pos = wordnet.VERB
        elif tag.startswith('JJ'):
            pos = wordnet.ADJ
        elif tag.startswith('RB'):
            pos = wordnet.ADV
        else:
            pos = None

        if pos:
            synonyms = wordnet.synsets(word, pos=pos)
            if synonyms:
                selected_synonym = synonyms[0].lemmas()[0].name()
                if selected_synonym == word and epoch == 1:
                    random.shuffle(synonyms)
                    selected_synonym = synonyms[0].lemmas()[0].name()
                simplified_words.append(selected_synonym)
            else:
                simplified_words.append(word)
        else:
            simplified_words.append(word)

    return ' '.join(simplified_words)
