import re

def modify_string(text):
    # Define patterns to match the words to be modified
    patterns = [
        (r"\s+'(?!s)\s*", "'s "),
        (r"\s*-\s*", "-"),
        (r"\s*\.", "."),
        (r"\s*,", ","),  # Remove only the space before the comma
        (r"\s+'s\b", "'s")

    ]

    # Apply the replacements using regular expressions
    for pattern, replacement in patterns:
        text = re.sub(pattern, replacement, text)

    return text

def replace_colon_with_period(text):
    # Remove extra whitespace at the beginning and end of the text
    text = text.strip()

    # Check if the text ends with a colon
    if text.endswith(":"):
        # Replace the trailing colon with a period
        text = text[:-1].strip() + "."

    # Replace " :" with ":"
    text = text.replace(" :", ":")

    # Remove space before the final period if present
    if text.endswith(" ."):
        text = text[:-1] + "."

    # Ensure the final period is directly after the last word
    if text and not text.endswith((".", "!", "?")):
        text += "."

    # Check for spaces before the final period with no alphabetic characters after the period
    match = re.search(r'\s+\.$', text)
    if match:
        # Remove the spaces before the period if no alphabetic characters follow
        if not re.search(r'[a-zA-Z]', text[match.start():]):
            text = text[:match.start()] + "."

    return text
