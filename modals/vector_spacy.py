import spacy

# Load the English NLP model from spaCy with word vectors
nlp = spacy.load('en_core_web_md')

def vector_simplify_text(input_text):
    # Process the input text using spaCy
    doc = nlp(input_text)

    simplified_words = []

    for token in doc:
        # Check if the token has a vector and is not a stop word
        if token.has_vector and not token.is_stop:
            # Get the most similar word based on word vectors
            most_similar_token = token.vocab.vectors.most_similar(
                token.vector.reshape(1, -1), n=1)[0][0]

            # Convert the most similar token to a string
            most_similar_token_str = nlp.vocab.strings[most_similar_token]

            # Check if the most similar token is not a stop word
            if not nlp.vocab[most_similar_token_str].is_stop:
                # Append the most similar word to the list of simplified words
                simplified_words.append(most_similar_token_str)
            else:
                # Append the original token text if the most similar word is a stop word
                simplified_words.append(token.text)
        else:
            # Append the original token text if it's a stop word or doesn't have a vector
            simplified_words.append(token.text)

    # Join the list of simplified words into a single string
    simplified_text = ' '.join(simplified_words)
    return simplified_text
