import torch
from transformers import BartForConditionalGeneration, BartTokenizer

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model_name = "sshleifer/distilbart-cnn-12-6"

# Load model and tokenizer
tokenizer = BartTokenizer.from_pretrained(model_name)
model = BartForConditionalGeneration.from_pretrained(model_name).to(device)

def gpt(input_text):
    inputs = tokenizer(input_text, max_length=1024, return_tensors="pt", truncation=True).to(device)

    # Generate summary
    with torch.no_grad():
        summary_ids = model.generate(inputs.input_ids, num_beams=4, max_length=250, early_stopping=True)

    # Decode and return the summary
    summary_text = tokenizer.decode(summary_ids[0], skip_special_tokens=True)
    return summary_text
