# from transformers import AutoTokenizer, T5ForConditionalGeneration
from transformers import BartForConditionalGeneration, BartTokenizer
# from modals.string_modifier import replace_colon_with_period
# from dotenv import load_dotenv
# import os
# load_dotenv()
#
# def gpt(input_text):
#     # Initialize the tokenizer and model
#     tokenizer = AutoTokenizer.from_pretrained(os.getenv('DNLP_DIR'))
#     model = T5ForConditionalGeneration.from_pretrained(os.getenv('DNLP_DIR'))
#     # Split the input text into sentences
#     sentences = input_text.split('. ')
#
#     # Initialize a list to store paraphrased sentences
#     paraphrased_sentences = []
#
#     # Iterate over each sentence, paraphrase it, and append to the list
#     for sentence in sentences:
#         input_sentence = 'paraphrase this: ' + sentence
#         input_ids = tokenizer(input_sentence, return_tensors="pt").input_ids
#         outputs = model.generate(input_ids, max_length=1500)
#         paraphrased_sentence = tokenizer.decode(outputs[0], skip_special_tokens=True)
#         paraphrased_sentences.append(replace_colon_with_period(paraphrased_sentence))
#
#     # Combine the paraphrased sentences into a single string
#     paraphrased_text = ' '.join(paraphrased_sentences)
#
#     return paraphrased_text


### 1st
from transformers import AutoTokenizer, T5ForConditionalGeneration
from modals.string_modifier import replace_colon_with_period
from dotenv import load_dotenv
import os

# Load environment variables
load_dotenv()

# Initialize the tokenizer and model globally to avoid repeated loading



def gramerly(input_text):

    tokenizer = AutoTokenizer.from_pretrained(os.getenv('DNLP'))
    model = T5ForConditionalGeneration.from_pretrained(os.getenv('DNLP'))

    # model.to('cuda:0')
    # Prepare the input text
    sentences = input_text.split('. ')
    # Add paraphrase prefixes to each sentence
    paraphrase_prompts = ['paraphrase this: ' + sentence for sentence in sentences]

    # Tokenize the input in batches
    batch = tokenizer(paraphrase_prompts, return_tensors="pt", padding=True, truncation=True)

    # Generate paraphrases in batch
    outputs = model.generate(batch.input_ids, max_length=256, num_beams=5, early_stopping=True)

    # Decode all outputs and replace colons with periods
    paraphrased_sentences = [
        replace_colon_with_period(tokenizer.decode(output, skip_special_tokens=True))
        for output in outputs
    ]

    # Combine the paraphrased sentences into a single string
    paraphrased_text = ' '.join(paraphrased_sentences)

    return paraphrased_text


######## 2nd


from transformers import BartTokenizer, BartForConditionalGeneration

# Load the tokenizer and model
# Initialize the tokenizer and model globally, on GPU if available

tokenizer = AutoTokenizer.from_pretrained(os.getenv('DNLP'))
model = T5ForConditionalGeneration.from_pretrained(os.getenv('DNLP'))


# Example usage
import torch

# device = 'cuda' if torch.cuda.is_available() else 'cpu'

import torch

def batchgpt(input_text, batch_size=8):
    # Split and prepare prompts
    sentences = input_text.split('. ')
    paraphrase_prompts = ['paraphrase this: ' + sentence for sentence in sentences]

    # Tokenize all prompts
    inputs = tokenizer(paraphrase_prompts, return_tensors="pt", padding=True, truncation=True)

    # Move model to device
    model.to(device)

    # Generate paraphrased sentences in batches
    paraphrased_sentences = []
    with torch.no_grad():
        for i in range(0, len(inputs['input_ids']), batch_size):
            batch_inputs = {
                'input_ids': inputs['input_ids'][i:i+batch_size].to(device),
                'attention_mask': inputs['attention_mask'][i:i+batch_size].to(device)
            }
            outputs = model.generate(**batch_inputs,
                                     max_length=256,
                                     num_beams=3,
                                     early_stopping=True)
            for output in outputs:
                paraphrased_sentences.append(replace_colon_with_period(tokenizer.decode(output, skip_special_tokens=True)))

            # Release GPU memory
            del batch_inputs
            torch.cuda.empty_cache()

    paraphrased_text = ' '.join(paraphrased_sentences)

    return paraphrased_text



def batchgrammargpt(input_text, batch_size=8):
    # Split and prepare prompts
    sentences = input_text.split('. ')
    paraphrase_prompts = ['fix the grammar: ' + sentence for sentence in sentences]

    # Tokenize all prompts
    inputs = tokenizer(paraphrase_prompts, return_tensors="pt", padding=True, truncation=True)

    # Move model to device
    model.to(device)

    # Generate paraphrased sentences in batches
    paraphrased_sentences = []
    with torch.no_grad():
        for i in range(0, len(inputs['input_ids']), batch_size):
            batch_inputs = {
                'input_ids': inputs['input_ids'][i:i+batch_size].to(device),
                'attention_mask': inputs['attention_mask'][i:i+batch_size].to(device)
            }
            outputs = model.generate(**batch_inputs,
                                     max_length=256,
                                     num_beams=3,
                                     early_stopping=True)
            for output in outputs:
                paraphrased_sentences.append(replace_colon_with_period(tokenizer.decode(output, skip_special_tokens=True)))

            # Release GPU memory
            del batch_inputs
            torch.cuda.empty_cache()

    paraphrased_text = ' '.join(paraphrased_sentences)

    return paraphrased_text


# def gpt(text):
#
#     # Move the model to the specified device
#     model_sshleifer.to(device)
#
#     # Encode the input text and generate the paraphrased output
#     inputs = tokenizer_sshleifer.encode("paraphrase: " + text, return_tensors="pt", max_length=512, truncation=True).to(device)
#     outputs = model_sshleifer.generate(inputs, max_length=150, num_beams=4, early_stopping=True)
#
#     # Decode the generated output into readable text
#     paraphrased_text = tokenizer.decode(outputs[0], skip_special_tokens=True)
#     return paraphrased_text
# tokenizer_sshleifer = BartTokenizer.from_pretrained("sshleifer/distilbart-cnn-12-6")
# model_sshleifer = BartForConditionalGeneration.from_pretrained("sshleifer/distilbart-cnn-12-6")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)