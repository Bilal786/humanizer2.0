import re
import json



def load_compound_words():
    try:
        with open('compound_words.json', 'r') as file:
            return json.load(file)
    except FileNotFoundError:
        return {}
def join_words(text):
    phrases = load_compound_words()
    # print(phrases)
    for phrase in phrases:
        joined_phrase = phrase.replace(" ", "_")
        # Use regex to replace whole phrases only
        text = re.sub(r'\b{}\b'.format(re.escape(phrase)), joined_phrase, text, flags=re.IGNORECASE)
    return text

# Define the phrases you want to join


# Example input text
# input_text = "Social media has a huge impact. The United Kingdom is a country in Europe."
# Process the text
def replace_compounds(input_text):
    output_text = join_words(input_text)
    return output_text
