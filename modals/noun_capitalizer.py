import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

def capitalize_nouns(input_text):
    tokens = word_tokenize(input_text)
    tagged = nltk.pos_tag(tokens)

    capitalized_words = []
    for word, tag in tagged:
        if tag.startswith('NN'):
            capitalized_words.append(word.capitalize())
        else:
            capitalized_words.append(word)

    capitalized_text = ' '.join(capitalized_words)
    return capitalized_text
