import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

def nlt_simplify_text(input_text):
    tokens = word_tokenize(input_text)
    tagged = nltk.pos_tag(tokens)

    simplified_words = []
    for word, tag in tagged:
        if tag.startswith('NN'):
            pos = wordnet.NOUN
            # pos = None
        elif tag.startswith('VB'):
            pos = wordnet.VERB
        elif tag.startswith('JJ'):
            pos = wordnet.ADJ
        elif tag.startswith('RB'):
            pos = wordnet.ADV
        else:
            pos = None

        if pos:
            synonyms = wordnet.synsets(word, pos=pos)
            if synonyms:
                # Shuffle the list of synonyms before selecting one
                # random.shuffle(synonyms)
                simplified_words.append(synonyms[0].lemmas()[0].name())
            else:
                simplified_words.append(word)

        else:
            simplified_words.append(word)

    simplified_text = ' '.join(simplified_words)
    return simplified_text
