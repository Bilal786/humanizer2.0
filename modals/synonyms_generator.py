import json
import openai
import re

# Load existing JSON file or create a new dictionary
def load_synonyms_dict():
    try:
        with open('synonyms.json', 'r') as file:
            return json.load(file)
    except FileNotFoundError:
        return {}

# OpenAI API key
openai.api_key = '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'

def clean_and_split_synonyms(synonyms_text):
    """Clean up and split synonyms by commas and newlines."""
    # Remove introductory sentence if it exists
    if "Here are some synonyms for" in synonyms_text:
        synonyms_text = re.sub(r"Here are some synonyms for [^:]+:\s*", "", synonyms_text)

    # Split by commas first
    raw_synonyms = synonyms_text.split(',')
    cleaned_synonyms = []
    for syn in raw_synonyms:
        # Further split by newlines if any
        split_synonyms = syn.split('\n')
        for split_syn in split_synonyms:
            cleaned_synonym = re.sub(r'^- ', '', split_syn).strip()  # Remove leading '- ' and strip spaces
            cleaned_synonym = re.sub(r'^\d+\.\s*', '', cleaned_synonym)  # Remove leading numbers and periods
            if cleaned_synonym:
                cleaned_synonyms.append(cleaned_synonym)
    return cleaned_synonyms

def get_synonyms_from_openai(word):
    prompt = f"Generate best fit synonyms for the word '{word}' separated by commas."
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": prompt}
        ],
        max_tokens=60,
        n=1,
        stop=None,
        temperature=0.5,
    )

    # Extract the response text
    synonyms_text = response.choices[0]['message']['content'].strip()
    print(f"GPT response for '{word}':", synonyms_text)

    # Clean and parse the response to get synonyms
    synonyms = clean_and_split_synonyms(synonyms_text)
    if word not in synonyms:  # Add the original word to the list if not already present
        synonyms.append(word)

    print(f"Parsed synonyms for '{word}':", synonyms)
    return synonyms  # Return the full list of synonyms

def save_synonyms_dict(synonyms_dict):
    with open('synonyms.json', 'w') as file:
        json.dump(synonyms_dict, file, indent=4)

def update_synonyms_dict(paragraph):
    synonyms_dict = load_synonyms_dict()
    words = paragraph.split()
    for word in words:
        word = word.strip(",.!?;:()").lower()  # Clean word
        if word not in synonyms_dict:
            print(f"Processing word: {word}")
            synonyms = get_synonyms_from_openai(word)
            print(f"Synonyms for '{word}': {synonyms}")
            synonyms_dict[word] = synonyms
    save_synonyms_dict(synonyms_dict)

# Example usage
# paragraph = "body"

# update_synonyms_dict(paragraph)
