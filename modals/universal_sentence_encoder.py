import tensorflow_hub as hub
import tensorflow as tf

# Define the input sentences
english_sentences = tf.constant(["dog", "Puppies are nice.", "I enjoy taking long walks along the beach with my dog."])

# Load the preprocessor and encoder from TensorFlow Hub
# Note: Check the URLs and ensure they are correct and accessible
preprocessor = hub.KerasLayer("https://kaggle.com/models/tensorflow/bert/TensorFlow2/en-uncased-preprocess/3")
encoder = hub.KerasLayer("https://www.kaggle.com/models/google/universal-sentence-encoder/TensorFlow2/cmlm-en-base/1")

# Apply the preprocessor and encoder to the input sentences
# Note: If the error persists, consider updating TensorFlow-Hub or reinstalling it
english_embeds = encoder(preprocessor(english_sentences))["default"]

# Print the embeddings
print(english_embeds)
