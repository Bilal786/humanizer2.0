def humanize_simplify_text(input_text):
    # Convert the input text to lowercase
    simplified_text = input_text.lower()

    # Replace underscores with spaces
    simplified_text = simplified_text.replace('_', ' ')

    # Capitalize the first letter of each sentence
    sentences = simplified_text.split('. ')  # Split into sentences
    capitalized_sentences = [s.capitalize() for s in sentences]  # Capitalize each sentence

    # Join the sentences back together with proper punctuation
    simplified_text = '. '.join(capitalized_sentences)

    return simplified_text