import re
import threading
from flask import Flask, request, jsonify, abort
from multiprocessing import Pool, cpu_count
from modals.nltk_text_parapharser import nlt_simplify_text
from modals.nltk_text_parapharser_v2 import nlt_simplify_text_v2
from modals.replace_compound_words import replace_compounds
# from modals.llma_spacy import spacy_simplify_text
from modals.humanize_converter import humanize_simplify_text
from modals.string_modifier import modify_string
from modals.noun_capitalizer import capitalize_nouns
from modals.summarizer import gpt
from modals.deep_neural_lang_processing import gramerly
from modals.synonyms_generator import update_synonyms_dict
from modals.deep_neural_lang_processing import batchgpt
from modals.deep_neural_lang_processing import batchgrammargpt
from dotenv import load_dotenv
import os
from modals.Qwen import qwen_func
# from modals.vector_spacy import vector_simplify_text

app = Flask(__name__)

# API_KEY = os.getenv('YOUR_SECRET_API_KEY')
# ALLOWED_IPS = ['192.168.18.82']  # Replace with your server's IP address
#
# def check_api_key():
#     api_key = request.headers.get('X-API-KEY')
#     if api_key != API_KEY:
#         abort(403)
#
# def check_ip_address():
#     client_ip = request.remote_addr
#     if client_ip not in ALLOWED_IPS:
#         abort(403)
#
# @app.before_request
# def before_request_func():
#     check_api_key()
#     check_ip_address()

@app.route('/api/post_data', methods=['POST'])
def post_data():
    input_text = request.form.get('content')
    paragraphs = input_text.split('\n')

    processed_paragraphs = []

    for paragraph in paragraphs:
        if bool(re.search(r'[a-zA-Z]', paragraph)):
            if len(paragraph.split(" ")) > 15:
                paragraph = nlt_simplify_text(paragraph)
                paragraph = paragraph.replace("_", " ")
                paragraph = gpt(paragraph)
            processed_paragraphs.append(paragraph)
    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({'essay': processed_text}), 200

@app.route('/api/humanizer2', methods=['POST'])
def humanizer2():
    input_text = request.form.get('content')
    paragraphs = input_text.split('\n')

    processed_paragraphs = []

    for paragraph in paragraphs:
        if bool(re.search(r'[a-zA-Z]', paragraph)):
            if len(paragraph.split(" ")) > 15:
                paragraph = nlt_simplify_text(paragraph)
                paragraph = paragraph.replace("_", " ")
                paragraph = gpt(paragraph)
                paragraph = batchgrammargpt(paragraph)
                paragraph = batchgpt(paragraph)
            processed_paragraphs.append(paragraph)
    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({'essay': processed_text}), 200

@app.route('/api/humanizer3', methods=['POST'])
def humanizer3():
    input_text = request.form.get('content')
    input_epoch = int(request.form.get('epoch', 2))
    paragraphs = input_text.split('\n')

    processed_paragraphs = []

    for paragraph in paragraphs:
        if bool(re.search(r'[a-zA-Z]', paragraph)):
            if len(paragraph.split(" ")) > 15:
                for _ in range(input_epoch):
                    paragraph = nlt_simplify_text(paragraph)
                    paragraph = paragraph.replace("_", " ")
                    paragraph = gpt(paragraph)
                    paragraph = batchgrammargpt(paragraph)
                    paragraph = batchgpt(paragraph)
                    if not bool(re.search(r'[a-zA-Z]', paragraph)):
                        break
            processed_paragraphs.append(paragraph)

    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({ 'essay': processed_text}), 200

def process_paragraph(paragraph_and_epoch):
    paragraph, input_epoch = paragraph_and_epoch
    if bool(re.search(r'[a-zA-Z]', paragraph)):
        if len(paragraph.split(" ")) > 15:
            for _ in range(input_epoch):
                paragraph = nlt_simplify_text(paragraph)
                paragraph = paragraph.replace("_", " ")
                paragraph = gpt(paragraph)
                paragraph = batchgrammargpt(paragraph)
                paragraph = batchgpt(paragraph)
                if not bool(re.search(r'[a-zA-Z]', paragraph)):
                    break
    return paragraph

@app.route('/api/humanizer4', methods=['POST'])
def humanizer4():
    input_text = request.form.get('content')
    input_epoch = int(request.form.get('epoch', 2))
    paragraphs = input_text.split('\n')

    with Pool(processes=cpu_count()) as pool:
        processed_paragraphs = pool.map(process_paragraph, [(paragraph, input_epoch) for paragraph in paragraphs])

    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({'essay': processed_text}), 200

@app.route('/api/humanizer5', methods=['POST'])
def humanizer5():
    input_text = request.form.get('content')
    input_epoch = int(request.form.get('epoch', 1))
    paragraphs = input_text.split('\n')

    processed_paragraphs = []

    for paragraph in paragraphs:
        # update_synonyms_dict(paragraph)
        # threading.Thread(target=update_synonyms_dict, args=(paragraph,)).start()
        if bool(re.search(r'[a-zA-Z]', paragraph)):
            if len(paragraph.split(" ")) > 15:
                for current_epoch in range(input_epoch):
                    paragraph = replace_compounds(paragraph)
                    paragraph = nlt_simplify_text_v2(paragraph,current_epoch)
                    paragraph = paragraph.replace("_", " ")
                    # paragraph = gpt(paragraph)
                    # paragraph = batchgrammargpt(paragraph)
                    paragraph = batchgpt(paragraph)
                    if not bool(re.search(r'[a-zA-Z]', paragraph)):
                        break
                paragraph = batchgrammargpt(paragraph)
            processed_paragraphs.append(paragraph)

    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({'essay': processed_text}), 200


@app.route('/api/paraphraser', methods=['POST'])
def paraphraser():
    input_text = request.form.get('content')
    input_epoch = int(request.form.get('epoch', 1))
    paragraphs = input_text.split('\n')

    processed_paragraphs = []

    for paragraph in paragraphs:
        if bool(re.search(r'[a-zA-Z]', paragraph)):
            if len(paragraph.split(" ")) > 15:
                for current_epoch in range(input_epoch):
                    paragraph = batchgpt(paragraph)
                    if not bool(re.search(r'[a-zA-Z]', paragraph)):
                        break
                paragraph = batchgrammargpt(paragraph)
            processed_paragraphs.append(paragraph)

    processed_text = '<br/><br/>'.join(processed_paragraphs)
    return jsonify({'essay': processed_text}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
